# UART used for console output.
# 1 by default.
CONSOLE_UART?=1

# Run link layer using SDMA.
ENABLE_SDMA?=0
